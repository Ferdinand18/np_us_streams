
export dirTab=/path/to/descriptor/tables
dirMdl=/path/to/RF/models
export dirOut=/path/to/output
dirgID=/path/to/id_tif

Nutlst=(TN TP TDN NO3 TDP)

for nut in ${Nutlst[@]}
do
for i  in 1 2 3 4
do
case $i in
	1)
	    season="Winter";;
	2)
	    season="Spring";;
	3)
	    season="Summer";;
	4)
	    season="Autumn";;
	*)
esac 

echo $nut $i $season | xargs -n 3 -P 8 bash -c $'	
nut=$1
i=$2
season=$3
export i 
export season 
export nut

R --vanilla --no-readline -q << \'EOF\'

#####################
## Initialization  ##
#####################

library(randomForestSRC)
library(geoR)
library(reshape)
library(data.table)
set.seed(1)

fun_invBoxCox <- function(x, lambda){
     if (lambda == 0) exp(x) 
     else (lambda*x + 1)^(1/lambda)
}

######################
# My data processing #
######################

dirmdl <- Sys.getenv("dirMdl")
dirtab <- Sys.getenv("dirTab")
dirout <- Sys.getenv("dirOut")
i <- Sys.getenv("i")
nut <- Sys.getenv("nut")

season <- Sys.getenv("season")

fmdl <- paste0("mdl_RF_US_",nut,"_",season)

load(paste0(dirmdl,fmdl,".rda"))

fdesc <- paste0(dirtab,"desc_season_",i,"_clean_short_split_")

df_stream <- fread(fdesc)

setDF(df_stream)

colnames(df_stream) <- c("gID","lu_avg_01", "lu_avg_02", "lu_avg_03", "lu_avg_04", "lu_avg_05", "lu_avg_06", "lu_avg_07", "lu_avg_08", "lu_avg_09", "lu_avg_10", "lu_avg_11", "lu_avg_12","prec","tmin","tmax","soil_avg_01","soil_avg_02","soil_avg_03","soil_avg_04","soil_avg_05","soil_avg_06","soil_avg_07","soil_avg_08","soil_avg_09","soil_avg_10","hydro_avg_01","hydro_avg_02","hydro_avg_03","hydro_avg_04","hydro_avg_05","hydro_avg_06","hydro_avg_07","hydro_avg_08","hydro_avg_09","hydro_avg_10","hydro_avg_11","hydro_avg_12","hydro_avg_13","hydro_avg_14","hydro_avg_15","hydro_avg_16","hydro_avg_17","hydro_avg_18","hydro_avg_19","dem_avg","slope_ave", "lentic_lotic01")

dim(df_stream)


#####################
# box-cox transform #
#####################

conditionalMean <- predict(lstMdl[[season]][["mdl"]], df_stream)

trans_pred <- fun_invBoxCox(conditionalMean$predicted,lambda=as.numeric(lstMdl[[season]][["lambda"]])) 

out_pred <- cbind(df_stream["gID"],trans_pred)

fout <- paste0("pred_mean_US_",nut,"_",season,".dat")
write.table(out_pred,file=paste0(dirout,"/",fout),row.names=FALSE,col.names=FALSE)

EOF

' _

done
done


for nut in ${Nutlst[@]}
do
 for season in Winter Spring Summer Autumn
do
echo $nut $season 

f_pred=pred_mean_US_${nut}_${season}.dat
out_tif=pred_mean_US_${nut}_${season}.tif
idTif=ID_raster_US_stream.tif

pkreclass -code ${dirOut}/${f_pred} -i ${dirgID}/${idTif} -m ${dirgID}/${idTif}  -msknodata 0 -nodata -1 -co COMPRESS=DEFLATE -co ZLEVEL=9 -ot Float32 -o ${dirOut}/${out_tif} 
done
done 
