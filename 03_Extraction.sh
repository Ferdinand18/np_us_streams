nutArr=(TN TP TDN TDP NO3)

###############################
# Split spanned into Monthly  #
###############################

echo ${nutArr[@]} | xargs -n 1 -P 8 bash -c $'
nut=$1 
fSnap=agg_mon_${nut}_post_snap.tsv
gawk -F "\t" -v ele=${nut} \'NR>1{print > ele"_"$3".tsv"}\' ${dirSnap}/${fSnap}
' _ 
mv *.tsv ${dirMon}

#######################
# Monthly Extraction  #
#######################
export mapDIR=/path/to/geo/tifs

# 12 months
I=1
N=12

for nut in ${nutArr[@]}
do
    for I in {1..12}
    do
	echo $nut $I 
    done
done | xargs -n 2 -P 8 bash -c $'
nut=$1
I=$2
printf -v J "%02d" $I
echo $nut, $J
awk -F "\t" \'{print $12, $13}\' ${dirDump}/Mon_Split/${nut}_${I}.tsv > ${dirDump}/Coord/coord_${nut}_${J}.dat

gdalbuildvrt -separate -overwrite ${dirDump}/VRT/${nut}_${J}.vrt  ${mapDIR}/landuse/avg/lu_?.tif ${mapDIR}/landuse/avg/lu_??.tif ${mapDIR}/climate/avg/prec_${I}.tif ${mapDIR}/climate/avg/tmin_${I}.tif ${mapDIR}/climate/avg/tmax_${I}.tif ${mapDIR}/soil/avg/*.tif ${mapDIR}/bioclim/avg/BIO_?.tif ${mapDIR}/bioclim/avg/BIO_??.tif ${mapDIR}/elevation/dem_avg.tif  ${mapDIR}/slope/slope_avg.tif  ${mapDIR}/stream_topo/stream_order_lakes0.tif 

gdallocationinfo -geoloc -valonly ${dirDump}/VRT/${nut}_${J}.vrt < ${dirDump}/Coord/coord_${nut}_${J}.dat > ${dirDump}/GeoVar/GeoVar_${nut}_${J}.dat 

paste -d "\t" ${dirDump}/Mon_Split/${nut}_${I}.tsv  <(awk \'{printf "%s" (NR%47==0?RS:FS) , $1}\' ${dirDump}/GeoVar/GeoVar_${nut}_${J}.dat | tr " " "\t") >  ${dirDump}/Mon_Cat/${nut}_${J}.tsv

egrep -v \'255|9999\' ${dirDump}/Mon_Cat/${nut}_${J}.tsv   > ${dirDump}/Mon_Cat/${nut}_${J}_snapped.tsv
' _


############
# Seasonal #
############

for nut in ${nutArr[@]}
do
    for Cson in ${CsonArr[@]}
    do
	echo $nut $Cson
    done
done | xargs -n 2 -P 8 bash -c $'
export nut=$1
export Cson=$2 
echo $nut $Cson
grep $Cson ${dirMon}/${nut}_*_snapped.tsv > ${dirCcat}/${nut}_${Cson}_snapped.tsv

export fname=${nut}_${Cson}_snapped
export dirCcat
export dirCagg

R --vanilla --no-readline -q << \'EOF\'
library(plyr)
nut=Sys.getenv(c(\'nut\'))
Cson=Sys.getenv(c(\'Cson\'))			  
fName=Sys.getenv(c(\'fname\'))
dDat=Sys.getenv(c(\'dirCcat\'))
dOut=Sys.getenv(c(\'dirCagg\'))

ds <- read.table(paste0(dDat,"/",fName,".tsv"),sep="\t")
colnames(ds) <- c("Long","Lat","mon","Cyear","season","N","mean","sd","cv","se","cat","sLong","sLat","lu_avg_01", "lu_avg_02", "lu_avg_03", "lu_avg_04", "lu_avg_05", "lu_avg_06", "lu_avg_07", "lu_avg_08", "lu_avg_09", "lu_avg_10", "lu_avg_11", "lu_avg_12","prec","tmin","tmax","soil_avg_01","soil_avg_02","soil_avg_03","soil_avg_04","soil_avg_05","soil_avg_06","soil_avg_07","soil_avg_08","soil_avg_09","soil_avg_10","hydro_avg_01","hydro_avg_02","hydro_avg_03","hydro_avg_04","hydro_avg_05","hydro_avg_06","hydro_avg_07","hydro_avg_08","hydro_avg_09","hydro_avg_10","hydro_avg_11","hydro_avg_12","hydro_avg_13","hydro_avg_14","hydro_avg_15","hydro_avg_16","hydro_avg_17","hydro_avg_18","hydro_avg_19","dem_avg","slope_ave", "lentic_lotic01")

datcols <- c("mean","lu_avg_01", "lu_avg_02", "lu_avg_03", "lu_avg_04", "lu_avg_05", "lu_avg_06", "lu_avg_07", "lu_avg_08", "lu_avg_09", "lu_avg_10", "lu_avg_11", "lu_avg_12","prec","tmin","tmax","soil_avg_01","soil_avg_02","soil_avg_03","soil_avg_04","soil_avg_05","soil_avg_06","soil_avg_07","soil_avg_08","soil_avg_09","soil_avg_10","hydro_avg_01","hydro_avg_02","hydro_avg_03","hydro_avg_04","hydro_avg_05","hydro_avg_06","hydro_avg_07","hydro_avg_08","hydro_avg_09","hydro_avg_10","hydro_avg_11","hydro_avg_12","hydro_avg_13","hydro_avg_14","hydro_avg_15","hydro_avg_16","hydro_avg_17","hydro_avg_18","hydro_avg_19","dem_avg","slope_ave", "lentic_lotic01")

ds <- subset(ds,cv<2)

grpcols <- c("sLong","sLat")
aggdat <- ddply(ds,grpcols,function(x) colMeans(x[datcols]))
dsOut <- paste0(paste0(dOut,"/","agg_",nut,"_",Cson,".tsv"));
write.table(aggdat,dsOut,row.names=FALSE,quote=FALSE,sep="\t")

EOF
' _

#################
# load location #
#################

export GISDBASE=/path/to/grassdb
export LOCATION=US_stream
export rcSuffix=US_stream

export GRASS_ADDON_BASE=$HOME/.grass7/addons
export GRASS_PAGER=cat
export GRASS_MESSAGE_FORMAT=plain
export GISBASE=/usr/local/grass-7.6.0
export PATH=$PATH:${GISBASE}/bin:${GISBASE}/scripts
export GISRC=${HOME}/.grass7/rc_${rcSuffix}
export GRASS_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
export GRASS_PYTHON=python 
export PYTHONPATH="$GISBASE/etc/python:$PYTHONPATH"
export GRASS_OVERWRITE=1

echo "########################"
echo  Welcome to GRASS
echo "########################"

g.gisenv 

echo "########################"
echo Start to use GRASS commands
echo "########################"

###################
# Kernel Fitting  #
###################
for nut in ${nutArr[@]}
do
    for Cson in ${CsonArr[@]}
    do
         echo $nut $Cson
    done
done | xargs -n 2 -P 8 bash -c $'   
nut=$1
Cson=$2	
fname=coord_${nut}_${Cson}	
awk \'BEGIN{FS=OFS="\t"}NR>1{print $1,$2}\' ${inputDir}/agg_${nut}_${Cson}.tsv > ${outputDir}/${fname}.tsv
    v.in.ascii in=${outputDir}/${fname}.tsv out=${fname} separator=tab columns="sLong  double precision, sLat double precision" x=1 y=2  --overwrite
     v.kernel input=${fname} output=${fname}_kernel radius=0.41666666
     v.out.ogr input=${fname} output=${outputDir}/${fname}.shp  type=point format=ESRI_Shapefile --overwrite
     r.out.gdal -f -c input=${fname}_kernel output=${outputDir}/${fname}_kernel.tif type=Float32 nodata=-1 --overwrite
' _

#############################
# Add Density to Attr. Tbl. #
#############################

export dirVar=/path/to/seasonal/obs
export dirDen=/path/to/density/kernel

for nut in ${nutArr[@]}
do
    for Cson in ${CsonArr[@]}
    do
         echo $nut $Cson
    done
done | xargs -n 2 -P 8 bash -c $'   
nut=$1
Cson=$2
echo "den" > ${dirDen}/density_${nut}_${Cson}.dat
gdallocationinfo -geoloc -valonly ${dirDen}/coord_${nut}_${Cson}_kernel.tif < ${dirDen}/coord_${nut}_${Cson}.tsv >> ${dirDen}/density_${nut}_${Cson}.dat 
paste -d "\t" ${dirVar}/agg_${nut}_${Cson}.tsv ${dirDen}/density_${nut}_${Cson}.dat > ${dirOut}/agg_${nut}_${Cson}.tsv
' _
