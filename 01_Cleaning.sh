
export dirDat=/path/to/downloaded/observation/data
export dirOut=/path/to/output 
export dirShp=/path/to/US/shp

#################################
# Thinning Stations and Results #
#################################

ls ${dirDat}/result_????.tsv | xargs -n 1 -P 8 bash -c $'
fwPath=$1
fName=$(basename $fwPath .tsv)
echo $fName
sed -n \'2,$p\' $fwPath | awk \'BEGIN{FS=OFS="\t"}{gsub(/\"/,"",$34);gsub(/[[:blank:]]/,"",$35);if(NF==63 && $22!="" && $34!="" && $34>0.0 && $35!="" && $35!~/None/ && $46!="" && $46!~/None/) print $22,$46,$32,$34,$35,$7,$8,$9,$10,$11,$12}\' > ${dirOut}/${fName}_slim.tsv
' _

##########################
# Building a Buffer Zone #
##########################

export bufval=10

ls ${dirDat}/station_????.tsv | xargs -n 1 -P 8 bash -c $'
fwPath=$1
fName=$(basename $fwPath .tsv)
echo $fName
sed -n \'2,$p\' ${fwPath}| awk \'BEGIN{FS=OFS="\t"}{gsub(" ","",$3);gsub("\\\"","",$12);gsub("\\\"","",$13);print $3,$12,$13}\'  > ${dirOut}/${fName}_slim.tsv
awk  \'BEGIN{FS=OFS="\t"}{if($2 ~ /^[0-9]+\\.*[0-9]*$/ && $3 ~ /^-[0-9]+\\.*[0-9]*$/) print $1, $3, $2}\'  ${dirOut}/${fName}_slim.tsv  >  ${dirOut}/${fName}_valid.tsv
awk  \'BEGIN{FS=OFS="\t"}{print $2, $3}\' ${dirOut}/${fName}_valid.tsv > ${dirOut}/coord_${fName}.tsv
# checking for buffer zone thickness
gdallocationinfo -geoloc -valonly  ${dirShp}/CONUS_buf${bufval}_msk.tif <  ${dirOut}/coord_${fName}.tsv   > ${dirOut}/coord_${fName}_chk.tsv 
paste -d "\t" ${dirOut}/${fName}_valid.tsv ${dirOut}/coord_${fName}_chk.tsv > ${dirOut}/${fName}_label.tsv
awk \'BEGIN{FS=OFS="\t"}{if ($NF==1) print }\' ${dirOut}/${fName}_label.tsv > ${dirOut}/${fName}_CONUS.tsv
sort -t$"\t" -k 1,1 ${dirOut}/${fName}_CONUS.tsv > ${dirOut}/${fName}_sorted.tsv
' _

################################
# Merging Station and Result  #
################################

for year in {1994..2018}
do
    echo $year
done | xargs -n 1 -P 4 bash -c $'
year=$1
echo $year
join -1 1 -2 1 -t$\'\t\' ${dirDat}/station_${year}_sorted.tsv  ${dirDat}/result_${year}_sorted.tsv > ${dirOut}/Merged_${year}.tsv
' _

###################
# Nutrients Split #
###################

awk 'BEGIN{
    FS=OFS="\t";
    ofTN="TN_all.tsv";
    ofTDN="TDN_all.tsv";
    ofTP="TP_all.tsv";
    ofTDP="TDP_all.tsv";
    ofNO3="NO3_all.tsv";
}
{
 if($5 == 00600) { print  > ofTN;
    }
 else if ($5 == 00602) { print  > ofTDN
    }
 else if ($5 == 00665) { print  > ofTP
    }
 else if ($5 == 00666) { print  > ofTDP
    }
 else if ($5 == 00618) { print  > ofNO3
    }
}' ${dirDat}/Merged_all.tsv

###########################
# Box-Cox Transformation  #
###########################

export nutArr=(TN TP NO3 TDP TDN)
export dirDat=/path/to/data
export dirOut=/path/to/output

echo ${nutArr[@]} | xargs -n 1 -P 4 bash -c $'
nut=$1
export dirDat
export dirOut
export nut

R --vanilla --no-readline -q << \'EOF\'

library(geoR)
library(moments)
set.seed(1)

fun_invBoxCox <- function(x, lambda){
     if (lambda == 0) exp(x) 
     else (lambda*x + 1)^(1/lambda)
}

fun_BoxCox <- function(x, lambda){
    x <- as.vector(x)
    if (lambda == 0) log(x)
    else (x^(lambda) - 1)/lambda
}

func_quantile <- function(vec_,alpha_){
  pa <- quantile(vec_, alpha_)
  pe <- quantile(vec_, 1-alpha_)
  return(c(pa,pe))
}

dirDat=Sys.getenv(c(\'dirDat\'))
dirOut=Sys.getenv(c(\'dirOut\'))
nut=Sys.getenv(c(\'nut\'))

fIn <- paste0(dirDat,"/",nut,"_slim.tsv")

dataIn <- read.table(fIn,sep="\t")
colnames(dataIn) <- c("ID","longitude","latitude","conc", "year", "mon", "day")

bcf <- boxcoxfit(dataIn[,"conc"])
lambda_bc1 <- bcf$lambda
dataIn[,"bc"] <- fun_BoxCox(dataIn[,"conc"],lambda_bc1)
dataIn[,"log"] <- log10(dataIn[,"conc"])
alpha <- 0.025
quant_bc <- func_quantile(dataIn$bc,alpha)
ds_bc <- subset(dataIn,bc>=quant_bc[[1]] & bc<=quant_bc[[2]])
ds_bc[,"log"] <- log10(ds_bc[,"conc"])
bcf <- boxcoxfit(ds_bc[,"conc"])
lambda_bc2 <- bcf$lambda
ds_bc[,"bc1"] <- fun_BoxCox(ds_bc[,"conc"],lambda_bc2)

quant_log <- func_quantile(dataIn$log,alpha)
ds_log <- subset(dataIn,bc>=quant_log[[1]] & bc<=quant_log[[2]])
bcf <- boxcoxfit(ds_log[,"conc"])
lambda_log1 <- bcf$lambda
ds_log[,"bc"] <- fun_BoxCox(ds_log[,"conc"],lambda_log1)
ds_log[,"log1"] <- log10(ds_log[,"conc"])

tup.skew <- c(skewness(dataIn$"conc"),skewness(ds_bc$"conc"),skewness(ds_log$"conc"),
	      skewness(dataIn$"bc"),skewness(ds_bc$"log"),skewness(ds_bc$"bc1"),
	      skewness(dataIn$"log"),skewness(ds_log$"bc"),skewness(ds_log$"log1"))

pngOut <- paste0(dirOut,"/Distr_Plt/","hist_QQ_TR_",nut,".png")
png(pngOut,height=9,width=18,unit="in",res=300)
par(mfrow = c(3,6),oma = c(0, 0, 2, 0))
hist(dataIn$conc,breaks=70,xlab="(Conc.)",main="Original")
legend("topright",inset=.02,legend=paste("skewness = ",round(tup.skew[1],2)))
qqnorm(dataIn$conc,xlab="(Conc.)")
qqline(dataIn$conc,xlab="(Conc.)")
hist(ds_bc$conc,breaks=70,xlab="(Conc.)",main="Original (BC subset)")
qqnorm(ds_bc$conc,xlab="(Conc.)")
qqline(ds_bc$conc,xlab="(Conc.)")
hist(ds_log$conc,breaks=70,xlab="(Conc.)",main="Original (Log subset)")
qqnorm(ds_log$conc,xlab="(Conc.)")
qqline(ds_log$conc,xlab="(Conc.)")
hist(dataIn$bc,breaks=70,xlab="transformed",main="Box-Cox Transformation")
legend("topright",inset=.02,legend=c(paste("skewness = ",round(tup.skew[4],2)),
			   paste("alpha = ",alpha),
			   paste("lambda = ",round(lambda_bc1,digits=3))),
		           bty="n")
qqnorm(dataIn$bc,xlab="Box-Cox transformed")
qqline(dataIn$bc)
hist(ds_bc$log,breaks=70,xlab="transformed",main="Log on BC subset")
qqnorm(ds_bc$log,xlab="Box-Cox transformed")
qqline(ds_bc$log)
hist(ds_bc$bc1,breaks=70,xlab="transformed",main="BC on BC subset")
legend("topright",inset=.02,legend=c(paste("alpha = ",alpha),
			   paste("lambda = ",round(lambda_bc2,digits=3))),
		           bty="n")
qqnorm(ds_bc$bc1,xlab="Box-Cox transformed")
qqline(ds_bc$bc1)
hist(dataIn$log,breaks=70,xlab="log(Conc.)",main="Log Transformation")
legend("topright",inset=.02,legend=paste("skewness = ",round(tup.skew[7],2)))
qqnorm(dataIn$log,xlab="log transformed")
qqline(dataIn$log)
hist(ds_log$bc,breaks=70,xlab="transformed",main="BC on Log subset")
legend("topright",inset=.02,legend=c(paste("alpha = ",alpha),
			   paste("lambda = ",round(lambda_log1,digits=3))),
		           bty="n")
qqnorm(ds_log$bc,xlab="Box-Cox transformed")
qqline(ds_log$bc)
hist(ds_log$log1,breaks=70,xlab="log(Conc.)",main="Log on Log subset")
qqnorm(ds_log$log1,xlab="log transformed")
qqline(ds_log$log1)
mtext(nut, outer = TRUE, cex = 1.2)
dev.off()

pngOut <- paste0(dirOut,"/Distr_Plt/","hist_QQ_BC_",nut,".png")
png(pngOut,height=6,width=12,unit="in",res=300)
par(mfrow = c(2,4),oma = c(0, 0, 2, 0))
hist(dataIn$conc,breaks=70,xlab="(Conc.)",main="Original")
legend("topright",inset=.02,legend=paste("skewness = ",round(tup.skew[1],2)))
qqnorm(dataIn$conc,xlab="(Conc.)")
qqline(dataIn$conc,xlab="(Conc.)")
hist(ds_bc$conc,breaks=70,xlab="(Conc.)",main="Original (BC subset)")
qqnorm(ds_bc$conc,xlab="(Conc.)")
qqline(ds_bc$conc,xlab="(Conc.)")
hist(dataIn$bc,breaks=70,xlab="transformed",main="Box-Cox Transformation")
legend("topright",inset=.02,legend=c(paste("skewness = ",round(tup.skew[4],2)),
			   paste("alpha = ",alpha),
			   paste("lambda = ",round(lambda_bc1,digits=3))),
		           bty="n")
qqnorm(dataIn$bc,xlab="Box-Cox transformed")
qqline(dataIn$bc)
hist(ds_bc$bc,breaks=70,xlab="(Conc.)",main="Box-Cox Trans. (BC subset)")
qqnorm(ds_bc$bc,xlab="(Box-Cox Trans.)")
qqline(ds_bc$bc,xlab="(Box-Cox Trans.)")
mtext(nut, outer = TRUE, cex = 1.2)
dev.off()

fOut <- paste0(dirOut,"/Proc_Data/",nut,"_bc.csv")
write.csv(ds_bc,fOut,quote=FALSE,row.names=FALSE)
fOut <- paste0(dirOut,"/Proc_Data/",nut,"_log.csv")
write.csv(ds_log,fOut,quote=FALSE,row.names=FALSE)

EOF

#################
# Station Split #
#################

for nut in ${types[@]}
do
    echo $nut
done | xargs -n 1 -P 8 bash -c $'
nut=$1
awk -F "," \'NR>1{print $2,$3}\'  ${dirDat}/${nut}_cson_lab.csv | uniq -c | sort -nr -k 1,1 > ${dirOut}/${nut}_stat_occur_count.dat
awk \'{if($1>3) print NR,$0}\' ${dirFreq}/${nut}_stat_occur_count.dat  > ${dirFreq}/${nut}_stat_occur_003.dat
lnum=0
while read line 
do
    flds=($line)
    rown=$(printf "%04d" ${flds[0]})
    nobs=$(printf "%04d" ${flds[1]})
    fname=${nut}_stat_${rown}_${nobs}
    echo $fname
    awk -v long=${flds[2]} -v lat=${flds[3]} \'BEGIN{FS=","}NR>1{if($2==long && $3==lat) print}\' ${dirDat}/${nut}_cson_lab.csv  | sort  -t$"," -n  -k 5,5 -k 6,6 -k 7,7 -k 4,4 | tr "," "\t" > ${dirSplit}/${fname}.tsv

    npt=$(printf "%04d" $(gawk -v outfile="${dirTrip}/${fname}_trip.tsv" \'BEGIN{
    FS=OFS="\t";
    count=0;
    buf_arr[1]="0\t 0\t 0\t 0\t 0\t 0\t 0\n";
}
{ year = $5;
   mon = $6;
   arrlen = length(buf_arr)
   split(buf_arr[arrlen],prev,"\t");
   if (prev[5]==year && prev[6]==mon)
       buf_arr[arrlen+1] = $0;
   else if (arrlen >= 3){
       count ++ 
       for (i=1;i<=arrlen;i++)
	   print buf_arr[i] > outfile
       delete buf_arr;
       buf_arr[1] = $0;
   }
   else if (arrlen < 3){
       delete buf_arr;
       buf_arr[1] = $0
   }
}
END{
    print count
})\' ${dirSplit}/${fname}.tsv))

    echo $npt
    if [ ! -s ${dirTrip}/${fname}_trip.tsv ]; then
    	rm -f ${dirTrip}/${fname}_trip.tsv
    else
    	mv ${dirTrip}/${fname}_trip.tsv ${dirTrip}/${fname}_${npt}.tsv
    fi
done < ${dirFreq}/${nut}_stat_occur_003.dat

find ${dirTrip} -name ${nut}_stat_????_????_????.tsv| xargs -I % cat %  > ${dirTrip}/${nut}_stat_temp.tsv

awk \'BEGIN{FS=OFS="\t"}
{ if($6==01){
        seayr = $5
        sea = "Winter"
    }
  else if ($6==02||$6==03||$6==04){
        seayr = $5
        sea = "Spring"
    }
  else if ($6==05||$6==06||$6==07){
        seayr = $5
        sea = "Summer"
    }
  else if ($6==08||$6==09||$6==10){
        seayr = $5
        sea = "Autumn"
    }
  else if($6==11||$6==12){
        seayr = $5 + 1
        sea = "Winter"
    }
  print $0, seayr, sea
}\' ${dirTrip}/${nut}_stat_temp.tsv > ${dirTrip}/${nut}_stat_all.tsv

rm -f ${dirTrip}/${nut}_stat_temp.tsv

' _

#####################################
# Seasonal Plots through All Years  #
#   joint violin (triplicate data)  #
#####################################

echo ${types[@]}   | xargs -n 1 -P 8 bash -c $'
export nut=$1
export fname=${nut}_stat_all
export dirDat
export dirPlt
export dirAgg
R --vanilla --no-readline -q << \'EOF\'
library(plyr)
library(ggplot2)
library(ggpubr)
library(tseries)
dPlt=Sys.getenv(c(\'dirPlt\'))
fName=Sys.getenv(c(\'fname\'))
dDat=Sys.getenv(c(\'dirDat\'))
dAgg=Sys.getenv(c(\'dirAgg\'))
nut=Sys.getenv(c(\'nut\'))
ds <- read.table(paste0(dDat,"/",fName,".tsv"),sep="\t")
colnames(ds) <- c("ID","Long","Lat","conc","year","mon","day","Cyear","season")
grpcols <- c("mon","Cyear","season")
aggmon <- ddply(ds,grpcols,summarize,
                N = length(conc), 
		mean = mean(conc),
		sd = sd(conc),
		cv = sd/mean,
                se = sd/sqrt(N)
	       )
aggmon <- subset(aggmon, Cyear!=2019)
grpcols <- c("season","Cyear")
aggsea <- ddply(ds, grpcols,summarize,
                N = length(conc), 
		mean = mean(conc),
		sd = sd(conc),
		cv = sd/mean,
                se = sd/sqrt(N)
	       )
aggsea <- subset(aggsea,Cyear!=2019)
pltlst <- list("Winter"=NULL,"Spring"=NULL,"Summer"=NULL,"Autumn"=NULL)
for (sea in names(pltlst))
    { pltlst[[sea]] <- ggplot(subset(aggmon,season==sea),aes(x=Cyear,y=mean,group=Cyear)) + 
      geom_violin(trim=FALSE, fill="#A4A4A4", color="darkred") + geom_boxplot(width=0.1) + theme_minimal() +
      geom_line(data=subset(aggsea,season==sea),color="blue",aes(x=Cyear,y=mean,group=1)) +
      scale_x_continuous(breaks=seq(1994,2018)) + labs(x="year",y="conc.") +
      theme(axis.text.x = element_text(angle = 45, hjust = 1));
}
pltnam <- paste0(dPlt,"/","violin_",nut,".pdf")
pdf(pltnam,width=32,height=8)
figure <- ggarrange(pltlst[[1]],pltlst[[2]],pltlst[[3]],pltlst[[4]],labels = names(pltlst), hjust=-1, vjust=3, ncol = 2, nrow = 2)
annotate_figure(figure,text_grob(nut, color = "black", face = "bold", size = 14))
dev.off()
grpcols <- c("Long","Lat","mon","Cyear","season")
aggGeo <- ddply(ds, grpcols,summarize,
                N = length(conc), 
		mean = mean(conc),
		sd = sd(conc),
		cv = sd/mean,
                se = sd/sqrt(N)
	       )
dsOut <- paste0(paste0(dAgg,"/","agg_mon_",nut,".tsv"));
write.table(subset(aggGeo,cv <= 0.5),dsOut,row.names=FALSE,quote=FALSE,sep="\t")
EOF
' _

