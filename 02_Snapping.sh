
UStif=/path/to/UStif
fStream=stream_order_lakes0.tif
fProj=stream_proj.tif

###########################
# creating a new location #
###########################


export GISDBASE=/path/to/grassdb
export LOCATION=US_stream
export rcSuffix=US_stream

rm -rf  ${GISDBASE}/${LOCATION} 
grass76 -text -e -c ${streamDir}/${fProj} ${GISDBASE}/${LOCATION} ${GISDBASE}

echo "LOCATION_NAME: ${LOCATION}"             > $HOME/.grass7/rc_$rcSuffix
echo "GISDBASE: ${GISDBASE}"                   >> $HOME/.grass7/rc_$rcSuffix
echo "MAPSET: PERMANENT"                       >> $HOME/.grass7/rc_$rcSuffix
echo "GRASS_GUI: text"                         >> $HOME/.grass7/rc_$rcSuffix

export GRASS_ADDON_BASE=$HOME/.grass7/addons
export GRASS_PAGER=cat
export GRASS_MESSAGE_FORMAT=plain
export GISBASE=/usr/local/grass-7.6.0
export PATH=$PATH:${GISBASE}/bin:${GISBASE}/scripts
export GISRC=${HOME}/.grass7/rc_${rcSuffix}
export GRASS_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
export GRASS_PYTHON=python 
export PYTHONPATH="$GISBASE/etc/python:$PYTHONPATH"
export GRASS_OVERWRITE=1

echo "########################"
echo  Welcome to GRASS
echo "########################"


r.in.gdal input=${UStif} output=UStif --overwrite
g.region raster=UStif
r.mask raster=UStif --overwrite
r.in.gdal input=${streamDir}/${fProj} output=stream_proj --overwrite

############
# Snapping #
############

nutArr=(TN TDN TP TDP NO3)

echo ${nutArr[@]} | xargs -n 1 -P 8 bash -c $'    
nut=$1
fObsVar=agg_mon_${nut}
    v.in.ascii in=${inputDir}/${fObsVar}.tsv out=${fObsVar} separator=tab columns="Long double precision, Lat double precision, mon integer, Cyear integer, season varchar(10), N integer, mean double precision, sd double precision, cv double precision, se double precision" x=1 y=2 skip=1 --overwrite
    rad=3
    ${GRASS_ADDON_BASE}/bin/r.stream.snap input=${fObsVar} output=${fObsVar}_post_snap_${rad} stream_rast=stream_proj radius=${rad} --overwrite
    v.out.ogr input=${fObsVar} output=${outputDir}/${fObsVar}.shp  type=point format=ESRI_Shapefile --overwrite
    v.out.ogr input=${fObsVar}_post_snap_${rad} output=${outputDir}/${fObsVar}_post_snap_${rad}.shp layer=2  type=point format=ESRI_Shapefile --overwrite
ogrinfo -al ${outputDir}/${fObsVar}_post_snap_${rad}.shp | egrep "cat |POINT" | awk \'BEGIN{printf "cat\\tsLong\\tsLat\\n"}{gsub(/[()]/,""); if(NR%2==0) printf("%f\\t%f\\n",$2,$3); else printf("%d\\t",$4)}\' > ${outputDir}/${fObsVar}_post_snap_coord.tsv 
paste -d "\t" ${inputDir}/${fObsVar}.tsv ${outputDir}/${fObsVar}_post_snap_coord.tsv > ${outputDir}/${fObsVar}_post_snap.tsv
rm ${outputDir}/${fObsVar}_post_snap_coord.tsv
' _


