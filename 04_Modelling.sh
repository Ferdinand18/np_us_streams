
export dirDat=/path/to/input
export dirOut=/path/to/output

nutLst=(TN NO3 TDN TP TDP)

echo ${nutLst[@]} | xargs -n 1 -P 8 bash -c $'
nut=$1
export dirDat
export dirOut
export nut

R --vanilla --no-readline -q << \'EOF\'


#####################
## Initialization  ##
#####################

library(randomForestSRC)
library(geoR)
library(plyr)
library(dplyr)
library(reshape)
library(cowplot)
library(moments)
library(ggplot2)
set.seed(1)

fun_invBoxCox <- function(x, lambda){
     if (lambda == 0) exp(x) 
     else (lambda*x + 1)^(1/lambda)
}

fun_BoxCox <- function(x, lambda){
    if (lambda == 0) log(x)
    else (x^(lambda) - 1)/lambda
}


######################
# My data processing #
######################

dirname=Sys.getenv(c(\'dirDat\'))
dirOut=Sys.getenv(c(\'dirOut\'))
nut=Sys.getenv(c(\'nut\'))

lstMdl <- vector(mode="list",length=4)
names(lstMdl) <- c("Winter","Spring","Summer","Autumn")


for (i in 1:4) {
fIn <- paste0(dirname,"/agg_",nut,"_",names(lstMdl)[[i]],".tsv")
aggdat <- read.table(fIn,header=TRUE,sep="\t")
aggdat$denGrp <- as.numeric(cut_number(aggdat$den,3))

dim(aggdat)

# box-cox transform
bcf <- boxcoxfit(aggdat[,"mean"])
lambda <- bcf$lambda
aggdat$"bcmean" <- fun_BoxCox(aggdat[,"mean"],lambda)


#####################################
# Regression post box-cox transform #
#####################################

train <- sample(1:nrow(aggdat),ceiling(0.5*nrow(aggdat)),prob=(aggdat$den))

####################
# With one unif RV #
####################

predictors <- c("lu_avg_01", "lu_avg_02", "lu_avg_03", "lu_avg_04", "lu_avg_05", "lu_avg_06", "lu_avg_07", "lu_avg_08", "lu_avg_09", "lu_avg_10", "lu_avg_11", "lu_avg_12","prec","tmin","tmax","soil_avg_01","soil_avg_02","soil_avg_03","soil_avg_04","soil_avg_05","soil_avg_06","soil_avg_07","soil_avg_08","soil_avg_09","soil_avg_10","hydro_avg_01","hydro_avg_02","hydro_avg_03","hydro_avg_04","hydro_avg_05","hydro_avg_06","hydro_avg_07","hydro_avg_08","hydro_avg_09","hydro_avg_10","hydro_avg_11","hydro_avg_12","hydro_avg_13","hydro_avg_14","hydro_avg_15","hydro_avg_16","hydro_avg_17","hydro_avg_18","hydro_avg_19","dem_avg","slope_ave", "lentic_lotic01")

rf <- rfsrc(as.formula(paste("bcmean~",paste(predictors,collapse="+"))),data=aggdat[train,],ntree=6000, case.wt=aggdat[train,"den"])

predAll <- predict(rf, aggdat[,predictors])
aggdat$pred_bc <- predAll$predicted
aggdat[train,"pred_bc"] <- rf$predicted.oob
aggdat[train,"grp"] <- "train"
aggdat[-train,"grp"] <- "test"
aggdat["pred_ibc"] <- fun_invBoxCox(aggdat["pred_bc"],lambda)


if (i==1){
    aggdat["season"] <- "Winter"   
   }
else if (i==2){
     aggdat["season"] <- "Spring"       
}   
else if (i==3){
     aggdat["season"] <- "Summer"       
}
else if (i==4){
     aggdat["season"] <- "Autumn"       
}	

png(paste0(dirOut,"/Regr_Plts/","scatter_",nut,"_",names(lstMdl)[i],".png"),height=600,width=600)
par(mfrow = c(2,2))
plot(aggdat[train,"bcmean"],aggdat[train,"pred_bc"],main="train")
plot(aggdat[train,"mean"],aggdat[train,"pred_ibc"],main="train(IBC)")
plot(aggdat[-train,"bcmean"],aggdat[-train,"pred_bc"],main="test")
plot(aggdat[-train,"mean"],aggdat[-train,"pred_ibc"],main="test(IBC)")
dev.off()

	
lstMdl[[i]] <- vector(mode="list",length=2)
lstMdl[[i]][["mdl"]] <- rf
lstMdl[[i]][["lambda"]] <- lambda

filename <- paste0("US_",nut,"_",names(lstMdl)[i])

mdl_out <- paste0(dirOut,"/Regr_Mdls/","mdl_RF_",filename,".rda")
save(lstMdl,file=mdl_out)

}

EOF

' _


